//
//  OperatorType.swift
//  Calculator
//
//  Created by Данил Войдилов on 30.04.2018.
//  Copyright © 2018 Данил Войдилов. All rights reserved.
//

import Foundation

public enum OperatorPriority: Int, Comparable {
    case addition, multiplication, power, suffix, brackets
    public static let min: OperatorPriority = .addition
    public static let max: OperatorPriority = .brackets
    public static func <(lhs: OperatorPriority, rhs: OperatorPriority) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}

public protocol OperatorTypeProtocol {
    associatedtype Value
    var symbol: String { get }
    var priority: OperatorPriority { get }
    var operation: (Value?, Value?) -> Value? { get }
}

public enum ArgumentsSide {
    case left, right, both
}

public enum OperatorType: String, OperatorTypeProtocol {
    case addition = "+"
    case subtraction = "-"
    case multiplication = "*"
    case fraction = "/"
    case power = "^", exp, abs, log, root, conj, mod, asinh, asin, sinh, sin, acosh, acos, cosh, cos, atanh, atan, tanh, tan, acoth, acot, coth, cot
    case fact = "!"
    
    public var symbol: String { return rawValue }
    
    public var priority: OperatorPriority {
        switch self {
        case .addition, .subtraction: return .addition
        case .multiplication, .fraction: return .multiplication
        case .exp, .abs, .conj, .asinh, .asin, .sinh, .sin, .acosh, .acos, .cosh, .cos, .atanh, .atan, .tanh, .tan, .acoth, .acot, .coth, .cot:
            return .suffix
        default: return .power
        }
    }
    
    public var operandsCount: Int {
        switch self {
        case .multiplication, .fraction, .power, .addition, .subtraction, .root, .log:
            return 2
        default:
            return 1
        }
    }
    
    public var argumentsSide: ArgumentsSide {
        switch self {
        case .addition, .subtraction, .multiplication, .fraction, .log, .root, .power, .mod:
            return .both
        case .fact:
            return .left
        case .exp, .abs, .conj, .asinh, .asin, .sinh, .sin, .acosh, .acos, .cosh, .cos, .atanh, .atan, .tanh, .tan, .acoth, .acot, .coth, .cot:
            return .right
        }
    }
    
    public var operation: (Complex?, Complex?) -> Complex? {
        switch self {
        case .addition: return { (left, right) in guard let r = right else { return nil }; return (left ?? 0) + r }
        case .subtraction: return { (left, right) in guard let r = right else { return nil }; return (left ?? 0) - r }
        case .multiplication: return { (left, right) in
            guard let r = right, let l = left else { return nil }
            return l * r }
        case .fraction: return { (left, right) in
            guard let r = right, let l = left else { return nil }
            return l / r }
        default: return { _,_ in return nil }
        }
    }
}
