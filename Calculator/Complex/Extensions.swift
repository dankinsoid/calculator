//
//  Extensions.swift
//  UniCountFrameWork
//
//  Created by Данил Войдилов on 29.04.2018.
//  Copyright © 2018 Данил. All rights reserved.
//

import Foundation

extension Double {
    public var i: Complex { return Complex(i: self) }
}
