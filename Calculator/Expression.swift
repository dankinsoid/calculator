//
//  Expression.swift
//  Calculator
//
//  Created by Данил Войдилов on 30.04.2018.
//  Copyright © 2018 Данил Войдилов. All rights reserved.
//

import Foundation

public protocol ExpressionProtocol: class, CustomStringConvertible {
    var value: Complex? { get }
    var type: ExpressionType { get }
    var priority: OperatorPriority { get }
    func encodeNext(prev: ExpressionProtocol?, next: inout [ExpressionProtocol]) -> ExpressionProtocol
}

public enum ExpressionType {
    case operand, operation
}

public enum Node: Equatable {
    case empty
    case left(ExpressionProtocol)
    case right(ExpressionProtocol)
    case both(left: ExpressionProtocol?, right: ExpressionProtocol?)
    
    public var left: ExpressionProtocol? {
        switch self {
        case .left(let value):
            return value
        case .both(let value, _):
            return value
        default:
            return nil
        }
    }
    
    public var right: ExpressionProtocol? {
        switch self {
        case .right(let value):
            return value
        case .both(_, let value):
            return value
        default:
            return nil
        }
    }
    
    public var isEmpty: Bool {
        switch self {
        case .empty:
            return true
        default:
            return false
        }
    }
    
    public static func ==(lhs: Node, rhs: Node) -> Bool {
        switch (lhs, rhs) {
        case (.empty, .empty), (.left(_), .left(_)), (.right(_), .right(_)):
            return true
        default:
            return false
        }
    }
}

public class Operand: ExpressionProtocol {
    
    public var description: String { return value?.description ?? "nil" }
    public var value: Complex?
    public let type: ExpressionType = .operand
    public let priority: OperatorPriority = .brackets
    public init(_ value: Complex) { self.value = value }
    
    public func encodeNext(prev: ExpressionProtocol?, next: inout [ExpressionProtocol]) -> ExpressionProtocol {
        return self
    }
}

public class Operator: ExpressionProtocol {
    public var description: String {
        return "(\(node.left == nil ? "" : "\(node.left!)")\(operation.rawValue)\(node.right == nil ? "" : "\(node.right!)"))"
    }
    public var operation: OperatorType
    public var priority: OperatorPriority
    public var hasBranches: Bool { return !node.isEmpty }
    public let type: ExpressionType = .operation
    public var node: Node = .empty
    public var value: Complex? {
        return count(node.left?.value, node.right?.value)
    }
    
    public init(_ operation: OperatorType, node: Node = .empty, priority: OperatorPriority? = nil) {
        self.operation = operation
        self.node = node
        self.priority = priority ?? operation.priority
    }
    
    public init?(_ string: String) {
        guard let op = (try? Operator.encode([])) as? Operator else { return nil }
        self.node = op.node
        self.operation = op.operation
        self.priority = op.priority
    }
    
    public init?(operator o: String) {
        guard let op = OperatorType(rawValue: o) else { return nil }
        self.node = .empty
        self.operation = op
        self.priority = op.priority
    }
    
    private func count(_ left: Complex?, _ right: Complex? = nil) -> Complex? {
        return operation.operation(left, right)
    }
}

extension Operator {
    public class func encode(_ array: [ExpressionProtocol]) throws -> ExpressionProtocol {
        guard array.count > 0 else { throw NSError() }
        guard array.count > 1 else { return array.first! }
        var root: ExpressionProtocol = array.first!
        for i in 0..<array.count-1 {
          //  try encodingStep(root: &root, current: array[i], next: array[i+1])
        }
        return root
    }
    
    public func encodeNext(prev: ExpressionProtocol?, next: inout [ExpressionProtocol], root: inout ExpressionProtocol?) -> ExpressionProtocol {
        guard !next.isEmpty else { return self }
        var nxt = next.first!
        next.removeFirst()
        if self.priority == .max && (self as? Operator)?.hasBranches != false {
            if (nxt as? Operator)?.hasBranches != false {
                nxt = Operator(.multiplication)
                next.insert(nxt, at: 0)
            }
            if (prev?.priority ?? .min) < nxt.priority {
                if (nxt as? Operator)?.operation.argumentsSide == .left || (nxt as? Operator)?.operation.argumentsSide == .both {
                    (nxt as? Operator)?.node = .both(left: self, right: nil)
                }
                return nxt.encodeNext(prev: self, next: &next)
            } else {
                return self
            }
            //return nxt.encodeNext(&root, next: &next)
        }
        if let op = self as? Operator {
            if !op.hasBranches && (op.operation.argumentsSide == .left || op.operation.argumentsSide == .both) {
                if (root?.priority ?? .min) < op.priority {
                    op.node = .both(left: prev, right: op.encodeNext(prev: self, next: &next, root: &root))
                }
            }
        }
        return self
        //5-6*5-3
        //
    }
}
//
//    fileprivate class func encodingStep(root: inout ExpressionProtocol, current: ExpressionProtocol, next: ExpressionProtocol) throws {
//        switch (root.type, next.type) {
//        case (.operand, .operation):
//            guard let cur = root as? Operand, let nxt = next as? Operator else { throw NSError() }
//            if nxt.branches == nil {
//                nxt.branches = (left: cur, right: nil)
//                root = next
//            } else {
//                root = Operator(.multiplication, branches: (left: cur, right: nil))
//                try encodingStep(root: &root, next)
//            }
//        case (.operation, .operand):
//            guard let cur = root as? Operator, next as? Operand != nil else { throw NSError() }
//            if !cur.hasBranches {
//                guard cur.branches != nil && cur.operation.operandsCount > 1 else { throw NSError() }
//                cur.branches?.right = next
//            } else {
//                if cur.operation.priority >= OperatorType.multiplication.priority {
//                    root = Operator(.multiplication, branches: (left: cur, right: next))
//                } else {
//                    guard cur.branches?.right != nil else { throw NSError() }
//                    cur.branches?.right = Operator(.multiplication, branches: (left: cur.branches!.right!, right: next))
//                }
//            }
//        case (.operand, .operand):
//            guard let cur = root as? Operand, next as? Operand != nil else { throw NSError() }
//            root = Operator(.multiplication, branches: (left: cur, right: next))
//        case (.operation, .operation):
//            guard let cur = root as? Operator, let nxt = next as? Operator else { throw NSError() }
//            if cur.hasBranches && ((current as? Operator)?.hasBranches ?? true) && nxt.branches != nil {
//                root = Operator(.multiplication, branches: (left: root, right: next))
//            } else {
//                if let c = current as? Operator {
//                    if cur.hasBranches && !c.hasBranches {
//                        if c.priority >= nxt.priority {
//                            guard cur.hasBranches else { throw NSError() }
//                            nxt.branches = (left: root, right: nxt.branches?.right)
//                            root = nxt
//                        } else {
//                            guard cur.operation.operandsCount > 1 else { throw NSError() }
//                            if cur.branches?.right != nil {
//                                nxt.branches = (left: cur.branches!.right!, right: nil)
//                            }
//                            cur.branches?.right = nxt
//                        }
//                    }
//                }
//                if cur.priority >= nxt.priority {
//                    guard cur.hasBranches else { throw NSError() }
//                    nxt.branches = (left: root, right: nxt.branches?.right)
//                    root = nxt
//                } else {
//                    guard cur.operation.operandsCount > 1 else { throw NSError() }
//                    if cur.branches?.right != nil {
//                        nxt.branches = (left: cur.branches!.right!, right: nil)
//                    }
//                    cur.branches?.right = nxt
//                }
//            }
//        }
//    }

extension String {
    public func toExpression() -> ExpressionProtocol? {
        if let operand = Double(self) { return Operand(Complex(operand)) }
        func arrayToExpr(_ any: AnyArray) -> ExpressionProtocol? {
            var expr: [ExpressionProtocol] = []
            for plane in any {
                if let a = plane as? AnyArray, let add = arrayToExpr(a) {
                    expr.append(add)
                } else if let str = plane as? String,
                    let exprs = str.simpleToExpr(),
                    let exp = try? Operator.encode(exprs) {
                    expr.append(exp)
                } else {
                    return nil
                }
            }
            let op = try? Operator.encode(expr)
            (op as? Operator)?.priority = .brackets
            return op
        }
        guard let array = splitBrackets() else { return nil }
        return arrayToExpr(array)
    }
    
    fileprivate func simpleToExpr() -> [ExpressionProtocol]? {
        var expr: [ExpressionProtocol] = []
        let ws = replacingOccurrences(of: " ", with: "")
        let parts = ws.divide(separatedBy: (CharacterSet.alphanumerics.union(["."])).inverted)
        guard parts.count > 0 else { return nil }
        for part in parts {
            if let opd = Double(part) {
                expr.append(Operand(Complex(opd)))
            } else if let opr = OperatorType(rawValue: part) {
                expr.append(Operator(opr))
            } else if part.contains("i") {
                let wi = part.replacingOccurrences(of: "i", with: "")
                let ni = part.components(separatedBy: "i").count - 1
                let c = Complex.i ^ Complex(ni)
                if wi == "" {
                    expr.append(Operand(c))
                } else if let d = Double(wi) {
                    expr.append(Operand(Complex(d, i: c.im)))
                } else {
                    return nil
                }
            } else {
                return nil
            }
        }
        return expr
    }
    
    public func divide(separatedBy set: CharacterSet) -> [String] {
        var result = [String]()
        var pos = startIndex
        while let range = rangeOfCharacter(from: set, range: pos..<endIndex) {
            if range.lowerBound != pos {
                result.append(String(self[pos..<range.lowerBound]))
            }
            result.append(String(self[range]))
            pos = range.upperBound
        }
        if pos != endIndex {
            result.append(String(self[pos..<endIndex]))
        }
        return result
    }
}

extension CharacterSet {
    public static let operators: CharacterSet = CharacterSet(charactersIn: "-+*/=÷%")
    public static let complexDigits: CharacterSet = CharacterSet(charactersIn: "0123456789i.")
}
