//
//  Classes.swift
//  MyFTR
//
//  Created by Данил Войдилов on 21.04.2018.
//  Copyright © 2018 Данил Войдилов. All rights reserved.
//

import Foundation

prefix operator ^

public prefix func ^<T>(_ right: Array<T>) -> ArrayClass<T> {
    let res: ArrayClass<T> = []
    right.forEach { res.append($0) }
    return res
}

public typealias AnyArray = ArrayClass<Any>
public class ArrayClass<Element>: Codable, RandomAccessCollection, MutableCollection, Collection, ExpressibleByArrayLiteral, CustomStringConvertible {
    public var description: String { return array.description }
    
    public typealias ArrayLiteralElement = Element
    
    private var array: [Element] = []
    public var capacity: Int { return array.capacity }
    public var startIndex: Int { return array.startIndex }
    public var endIndex: Int { return array.endIndex }
    public var unwrap: SafeArray<Element> { return SafeArray(array) }
    public var container: [Element] { return array }

    public subscript (i: Int) -> Element {
        get { return array[i] }
        set { array[i] = newValue }
    }
    
    public init() {}
    public required init(from decoder: Decoder) throws { try array = [Element](from: decoder) }
    public required init(arrayLiteral elements: Element...) { array = elements }
    
    public func encode(to encoder: Encoder) throws { try array.encode(to: encoder) }
    public func index(after i: Int) -> Int { return array.index(after: i) }
    public func index(before i: Int) -> Int { return array.index(before: i) }
    public func add(_ value: Element) { array.append(value) }
    public func append(_ value: Element) { array.append(value) }
    public func copy() -> [Element] { return array }
    public func insert(_ value : Element, at index: Int) { array.insert(value, at: index) }
    public func remove(at index: Int) -> Element { return array.remove(at: index) }
    public func removeLast(n: Int = 1) { array.removeLast(n) }
    public func removeFirst(n: Int = 1) { array.removeFirst(n) }
    public func removeAll(keepingCapacity: Bool = false) { array.removeAll(keepingCapacity: keepingCapacity) }
}

public struct SafeArray<T>: ExpressibleByArrayLiteral {
    
    public typealias ArrayLiteralElement = T
    
    public var array = Array<T>()
    
    public init(arrayLiteral elements: T...) {
        self.array = elements
    }
    
    public init(_ elements: [T]) {
        self.array = elements
    }
    
    public subscript (i: Int) -> T? {
        guard i > -1 && i < array.count else {return nil}
        return array[i]
    }
}

public extension Array {
    var unwind: SafeArray<Element> { return SafeArray(self) }
}
