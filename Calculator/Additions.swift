//
//  Additions.swift
//  Calculator
//
//  Created by Данил Войдилов on 30.04.2018.
//  Copyright © 2018 Данил Войдилов. All rights reserved.
//

import Foundation

infix operator ±
postfix operator %

extension BinaryInteger where Stride: SignedInteger, Self.Magnitude == Self {
    public static func ±(lhs: Self, rhs: Self) -> CountableClosedRange<Self> {
        return (lhs - rhs.magnitude)...(lhs + rhs.magnitude)
    }
}

extension SignedInteger where Stride: SignedInteger {
    public static func ±(lhs: Self, rhs: Self) -> CountableClosedRange<Self> {
        let m = abs(rhs)
        return (lhs - m)...(lhs + m)
    }
}

extension FloatingPoint {
    public static func ±(lhs: Self, rhs: Self) -> ClosedRange<Self> {
        let m = abs(rhs)
        return (lhs - m)...(lhs + m)
    }
}

extension Int {
    public static func ±(lhs: Int, rhs: Int) -> CountableClosedRange<Int> {
        let m = abs(rhs)
        return (lhs - m)...(lhs + m)
    }
}

extension Double {
    public static postfix func %(left: Double) -> Double {
        return left / 100.0
    }
}

extension String {
   
    public func splitBrackets(_ left: Character = "(", _ right: Character = ")") -> AnyArray? {
        guard self.contains(left) || self.contains(right) else { return ^[self] }
        var i = 0
        var n = 0
        let array = Array(characters)
        let result = ^[Any]()
        var current = result
        var string = ""
        var inds: [Int] = [0]
        while n >= 0 && i < array.count {
            if array[i] == left || array[i] == right {
                if !string.isEmpty { current.append(string) }
                if array[i] == left {
                    inds[inds.count - 1] = current.count
                    current.append(^[Any]())
                    inds.append(0)
                    current = current.last! as! AnyArray
                    n += 1
                } else {
                    inds.removeLast()
                    current = result
                    if inds.count > 1 {
                        for i in inds[0..<(inds.count - 1)] { current = current[i] as! AnyArray }
                    }
                    n -= 1
                }
                string = ""
            } else {
                string += String(array[i])
            }
            i += 1
        }
        guard i == array.count && n == 0 else { return nil }
        if !string.isEmpty { current.append(string) }
        return result
    }
}
